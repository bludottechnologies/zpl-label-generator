export interface LabelConfig {
  dpmm: number;
  dpi: number;
  widthMM: number;
  heightMM: number;
  defaultLineSpacing: number;
  defaultPointSize: number;
  defaultFont: string;
  boldFont: string;
  marginsMM: MarginsMM;
}

export interface MarginsMM {
  top: number;
  right: number;
  bottom: number;
  left: number;
}

export interface SimpleTable {
  config: {
    leftWidthPercent: number;
    rightWidthPercent: number;
  };
  rows: tableRowConfig[];
}

interface tableRowConfig {
  left: tableFieldConfig;
  right: tableFieldConfig;
}

interface tableFieldConfig {
  value: string;
  pointSize: number;
  lines: number;
  font?: string;
}

export enum Alignment {
  LEFT = 'L',
  CENTER = 'C',
  RIGHT = 'R',
  JUSTIFIED = 'J',
}
