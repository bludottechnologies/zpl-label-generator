import { fontZeroRatios, fontARatios, symbols } from "./globals";

export const getCharWidthRatio = (char: string, font: string, pointSize: number): number => {
  const defaultRatio = 0.48;
  let ratio: any = defaultRatio;

  if (font === "0") {
    ratio = fontZeroRatios.get(char) ? fontZeroRatios.get(char) : defaultRatio;
  } else if (font === "A") {
    ratio = fontARatios.get(pointSize) ? fontARatios.get(pointSize) : defaultRatio;
  } else if (font === "custom") {
    ratio = fontZeroRatios.get(char) ? fontZeroRatios.get(char) : defaultRatio;
  } else {
    ratio = 0.77;
  }

  return ratio;
};

export const ptToMM = (pointSize: number): number => {
  return pointSize * 0.3527;
};

export const replaceSpecialCharsInFB = (data: string) => {
  let formattedData = data;

  for (let i = 0; i < symbols.length; i++) {
    const thisSymbol = symbols[i];
    formattedData = formattedData.split(thisSymbol.char).join("_9c");
  }
  return formattedData;
};
