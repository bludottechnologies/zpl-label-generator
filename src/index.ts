import { Label } from './labels';
import { LabelConfig, Alignment, SimpleTable } from './models/labels';

export { Label, LabelConfig, Alignment, SimpleTable };

console.log('%c ZPL Label Generator ', 'background: #bf0000; color: #ffffff');

console.log(
  `%c 
███████╗██████╗ ██╗     
╚══███╔╝██╔══██╗██║     
  ███╔╝ ██████╔╝██║     
 ███╔╝  ██╔═══╝ ██║     
███████╗██║     ███████╗
╚══════╝╚═╝     ╚══════╝
`,
  'color: #bf0000'
);
