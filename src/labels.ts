import { LabelConfig, Alignment, SimpleTable } from './models/labels';
import { getCharWidthRatio, replaceSpecialCharsInFB } from './utilities';
import { symbols, formattingChars } from './globals';

export class Label {
  private config: LabelConfig;
  private zplcommands: string = '';
  private fieldYposition: number;

  constructor(config: LabelConfig) {
    this.config = config;
    this.fieldYposition = this.getLabelStartPosition();
  }

  // getters & setters

  private getLabelConfig = (): any => {
    return this.config;
  };

  private getLabelStartPosition = (): any => {
    return this.mmToDots(this.config.marginsMM.top);
  };

  private getFieldYposition = (): number => {
    return this.fieldYposition;
  };

  private setFieldYposition = (dots: number) => {
    this.fieldYposition += dots;
  };

  private getFieldXposition = (): number => {
    return this.mmToDots(this.config.marginsMM.left);
  };

  private getAvailablePageWidth = (): number => {
    return this.mmToDots(this.config.widthMM - this.config.marginsMM.left - this.config.marginsMM.right);
  };

  // conversions

  private mmToDots = (mm: number): number => {
    const result = mm * this.config.dpmm;
    return result;
  };

  private ptToDots = (pointSize: number): number => {
    const dots = Math.round((pointSize * this.config.dpi) / 72);
    return dots;
  };

  private calculateWordWidthInDots = (word: string, pointSize: number, font: string): number => {
    let wordWidthInDots = 0;

    const chars = Array.from(word);

    for (let j = 0; j < chars.length; j++) {
      const thisChar = chars[j];
      const ratio = getCharWidthRatio(thisChar, font, pointSize);
      const charWidthInDots = this.ptToDots(pointSize) * ratio;
      wordWidthInDots += charWidthInDots;
    }

    return wordWidthInDots;
  };

  // zpl

  private appendItem = (item: string) => {
    this.zplcommands += `${item}\n`;
  };

  private writeFieldOrigin = (x: number, y: number): string => {
    return `^FO${x},${y}`;
  };

  private writeFieldBlock = (width: number, lines: number, alignment: Alignment): string => {
    return `^FB${width},${lines},0,${alignment},0`;
  };

  private writeFont = (pointSize: number): string => {
    let font = this.config.defaultFont;
    let fontString = `^A${font},${this.ptToDots(pointSize)},${this.ptToDots(pointSize)}`;
    return fontString;
  };

  private changeFont = (pointSize: number, font: string): string => {
    let fontString = `^CF${font},${this.ptToDots(pointSize)}`;

    if (font === 'custom') {
      fontString = `^A@,${this.ptToDots(pointSize)},${this.ptToDots(pointSize)},TT0003M_^`;
    }
    return fontString;
  };

  private writeTextWithinFieldBlock = (text: string, pointSize: number, lines: number = 1, newFont: string, setYposition = true): string => {
    const singleLineHeight = this.ptToDots(pointSize) * this.config.defaultLineSpacing;
    const font = this.changeFont(pointSize, newFont);
    if (setYposition) {
      this.setFieldYposition(singleLineHeight * lines);
    }
    return `${font}^FH_^FD${text}^FS`;
  };

  // public zpl builder functions

  generateZPL = (): string => {
    const body = `^XA${this.zplcommands}^XZ`;

    return body;
  };

  addSpacingMm = (mm: number) => {
    this.setFieldYposition(this.mmToDots(mm));
  };

  addFieldBlock = (
    data: string,
    pointSize: number = this.ptToDots(this.config.defaultPointSize),
    lines: number,
    alignment: Alignment,
    newFont = this.config.defaultFont,
    setYposition = true,
    percentWidth = 100,
    xStart = 0,
    paddingLeft = 0,
    paddingRight = 0
  ) => {
    let formattedData = replaceSpecialCharsInFB(data);
    let commands = '';

    const xPosition = xStart > 0 ? xStart : this.getFieldXposition();

    commands += this.writeFieldOrigin(xPosition + paddingLeft, this.getFieldYposition());
    commands += this.writeFieldBlock((this.getAvailablePageWidth() * percentWidth) / 100 - paddingLeft - paddingRight, lines, alignment);
    commands += this.writeTextWithinFieldBlock(formattedData, pointSize, lines, newFont, setYposition);

    this.appendItem(commands);
  };

  addText = (data: string, pointSize: number = this.ptToDots(this.config.defaultPointSize), newFont = this.config.defaultFont) => {
    let normalFont = this.changeFont(pointSize, newFont);
    let boldFont = this.changeFont(pointSize, this.config.boldFont);

    let singleLineHeight = this.ptToDots(pointSize) * this.config.defaultLineSpacing;

    if (newFont === 'A') {
      singleLineHeight = pointSize * this.config.defaultLineSpacing * 4;
    }

    const availableWidth = this.getAvailablePageWidth();

    let xPosition = this.getFieldXposition();
    // console.log('xPosition', xPosition);

    const words = data.split(' ');

    let text = '';
    let lineLengthInDots = 0;
    let boldCharStatus = false;

    for (let i = 0; i < words.length; i++) {
      let thisWord = `${words[i]}`;

      if (i < words.length - 1) {
        thisWord += ' ';
      }

      let wordWidthInDots = this.calculateWordWidthInDots(thisWord, pointSize, newFont);

      const chars = Array.from(thisWord);

      for (let j = 0; j < chars.length; j++) {
        if (chars[j] === formattingChars.bold[0] && chars[j + 1] === formattingChars.bold[1]) {
          boldCharStatus = !boldCharStatus;
          chars.splice(j, 2);
        }

        const thisChar = chars[j];

        const ratio = getCharWidthRatio(thisChar, newFont, pointSize);

        const charWidthInDots = this.ptToDots(pointSize) * ratio;

        if (lineLengthInDots + wordWidthInDots > availableWidth) {
          lineLengthInDots = 0;
          this.setFieldYposition(singleLineHeight * 1);

          xPosition = this.getFieldXposition();
        }

        const yPosition = this.getFieldYposition();

        const specialSymbol = symbols.find((element) => element.char === thisChar);

        const fontToUse = boldCharStatus ? boldFont : normalFont;

        if (specialSymbol) {
          text += `${this.writeFieldOrigin(xPosition, yPosition)}${fontToUse}^FH_^FD_${specialSymbol.zpl}^FS`;
        } else {
          text += `${this.writeFieldOrigin(xPosition, yPosition)}${fontToUse}^FD${thisChar}^FS`;
        }

        xPosition += charWidthInDots;
      }
      lineLengthInDots += wordWidthInDots;
    }
    this.setFieldYposition(singleLineHeight * 1);

    this.appendItem(text);
  };

  writeGraphicBox(x: number, y: number, w: number, h: number) {
    return `^FO${x},${y}^GB${w},${h},2^FS`;
  }

  addBarcode(ean: number, w: number = 3, h: number = 100, offsetLeftMm = 0) {
    let barcode = '';

    const xPosition = this.getFieldXposition();
    const yPosition = this.getFieldYposition();

    barcode += `^BY${w},3,${h}`;
    barcode += this.writeFieldOrigin(xPosition + this.mmToDots(offsetLeftMm), yPosition);
    barcode += `^BC^FD${ean}^FS`;

    this.setFieldYposition(h + w * 14);

    this.appendItem(barcode);
  }

  addSimpleTable(tableData: SimpleTable) {
    const rightXStartPosition = (this.getAvailablePageWidth() * tableData.config.leftWidthPercent) / 100 + this.mmToDots(this.config.marginsMM.left);

    const tableLeft = this.getFieldXposition();
    const tableRight = this.getAvailablePageWidth() + this.mmToDots(this.config.marginsMM.left);
    const tableTop = this.getFieldYposition();

    let otherCommands = '';

    // this.addFieldBlock(tableData.headerLeft.value, tableData.headerLeft.pointSize, tableData.headerLeft.lines, Alignment.LEFT, this.config.defaultFont, false, tableData.config.leftWidthPercent);
    // this.addFieldBlock(tableData.headerRight.value, tableData.headerRight.pointSize, tableData.headerRight.lines, Alignment.RIGHT, this.config.defaultFont, true, tableData.config.rightWidthPercent, rightXposition);

    this.addSpacingMm(2);

    for (let i = 0; i < tableData.rows.length; i++) {
      const thisRow = tableData.rows[i];
      this.addFieldBlock(
        thisRow.left.value,
        thisRow.left.pointSize,
        thisRow.left.lines,
        Alignment.LEFT,
        thisRow.left.font ? thisRow.left.font : this.config.defaultFont,
        false,
        tableData.config.leftWidthPercent,
        0,
        this.mmToDots(2)
      );
      this.addFieldBlock(
        thisRow.right.value,
        thisRow.right.pointSize,
        thisRow.right.lines,
        Alignment.RIGHT,
        thisRow.right.font ? thisRow.right.font : this.config.defaultFont,
        true,
        tableData.config.rightWidthPercent,
        rightXStartPosition,
        0,
        this.mmToDots(2)
      );
    }

    this.addSpacingMm(2);

    // y postion gets changed after above commands
    const tableBottom = this.getFieldYposition();

    otherCommands += this.writeGraphicBox(tableLeft, tableTop, tableRight - tableLeft, tableBottom - tableTop);

    // otherCommands += `${this.writeFieldOrigin(1, 50)}^FD${marker}^FS`;
    // otherCommands += `${this.writeFieldOrigin(50, 1)}^FD${marker}^FS`;
    // otherCommands += `${this.writeFieldOrigin(50, 50)}^FD${marker}^FS`;
    this.appendItem(otherCommands);
  }
}
