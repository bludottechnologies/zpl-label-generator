# **ZPL Label Generator**

JS code to build valid ZPL commands to print text at any point size. All fields will occupy the full label width, text will wrap and the next field will print relative to the previous field.

No origins etc need to be set, just configure label size and margins in _config.js_

### **Usage:**

**Install via NPM:**

    $ npm install zpl-label-generator

**Import:**

    import { Label, Alignment, LabelConfig } from 'zpl-label-generator';

    // 6dpmm = 152dpi
    // 8dpmm = 203dpi
    // 12dpmm = 300dpi
    // 24dpmm = 600dpi

    config: LabelConfig = {
        dpmm: 8,
        dpi: 203,
        widthMM: 60,
        heightMM: 151,
        defaultLineSpacing: 1,
        defaultPointSize: 6,
        defaultFont: '0',
        marginsMM: {
            top: 10,
            right: 4,
            bottom: 10,
            left: 4,
        },
    };

    label = new Label(this.config);

    const sampleText = `Marty you gotta come back with me. Don't worry, I'll take care of the lightning, you take care of your pop. By the way, what happened today, did he ask her out? Over there, on my hope chest. I've never seen purple underwear before, Calvin. Don't pay any attention to him, he's in one of his moods. Sam, quit fiddling with that thing, come in here to dinner. Now let's see, you already know Lorraine, this is Milton, this is Sally, that's Toby, and over there in the playpen is little baby Joey. You're gonna be in the car with her.`;

    this.label.addText('Back to the Future', 20);
    this.label.addText('1985', 10);
    this.label.addSpacingMm(5);
    this.label.addText('Synopsis', 10);
    this.label.addSpacingMm(2);
    this.label.addText(sampleText, 6);
    this.label.addSpacingMm(2);
    this.label.addFieldBlock(sampleText, 6, 9, Alignment.JUSTIFIED);
    this.label.addSpacingMm(2);
    this.label.addText(sampleText, 6);
    this.label.addText('End of Label', 10);

    this.zpl = this.label.generateZPL();

**addText(data: string, pointSize: number)**

Allows text to be added similar to standard ZPL Field Block but provides dynamic height.

**addSpacingMm(mm: number)**

Will add spacing between fields (in mm).

**addFieldBlock(data: string, pointSize: number, lines: number, alignment: Alignment)**

Will add text within a field block. Field block allows for text alignment (see typings below for available options), but you must specify number of lines to show.

**generateZPL()**

Once all content has been added, this will return the ZPL code.

### **Typings / Enums:**

**LabelConfig**

    LabelConfig {
        dpmm: number;
        dpi: number;
        widthMM: number;
        heightMM: number;
        defaultLineSpacing: number;
        defaultPointSize: number;
        defaultFont: string;
        marginsMM: {
            top: number;
            right: number;
            bottom: number;
            left: number;
        };
    }

**Alignment**

    Alignment {
        LEFT = "L",
        CENTER = "C",
        RIGHT = "R",
        JUSTIFIED = "J",
    }
